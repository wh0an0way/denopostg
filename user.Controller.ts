import { Router } from "https://deno.land/x/oak/mod.ts";
import db from "./db.ts";

const router = new Router();

router
  .get("/users", async (ctx) => {
    try {
      const result = await db.query({
        text: 'SELECT * FROM "user";',
      })
      ctx.response.body = result.rowsOfObjects();
    } catch (err) {
      console.log("RiP");
      ctx.throw(err);
  }
  })
  .get("/users/:id", (ctx) => {
    if (ctx.params && ctx.params.id) {
      const user = {
        id: ctx.params.id,
        name: "Emil Nashef",
        email: "fakeemaildonttryxD@protonmail.ch",
      };
      ctx.response.body = user;
    }
  });

export default router;
