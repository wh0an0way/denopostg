import { Application } from "https://deno.land/x/oak/mod.ts";
import UserController from "./user.Controller.ts";
const app = new Application();

// Logger

// Hello World!

app.use(async (ctx, next) => {
  await next();
  const rt = ctx.response.headers.get("X-Response-Time");
  console.log(`${ctx.request.method} ${ctx.request.url} - ${rt}`);
});

// Timing
app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  ctx.response.headers.set("X-Response-Time", `${ms}ms`);
});

app.use(UserController.routes());
app.use(UserController.allowedMethods());

app.use((ctx) => {
  ctx.response.body = "Hello World!";
});
console.log(`🚀 localhost:8000`);
await app.listen({ port: 8000 });
