import { Client } from "https://deno.land/x/postgres/mod.ts";

const client = new Client({
    user: "docker",
    database: "deno",
    hostname: "127.0.0.1",
    port: 25432,
    password: "docker",
  });
  await client.connect();


  export default client;